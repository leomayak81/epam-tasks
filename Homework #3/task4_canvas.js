const canvas = document.querySelector('.canvas');
const context = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 800;
let figureArray = [];


class Figure {
    constructor() {
        this.size = Math.random() * 100;
        this.x = 0;
        this.y = 0;
        this.speed = 5;
        this.dx = 0;
        this.dy = 0;
        this.color = 'rgb(' + (Math.floor(Math.random() * 256)) +
            ',' + (Math.floor(Math.random() * 256)) + ','
            + (Math.floor(Math.random() * 256)) + ')';
    }
    get area() {
        return this.calcArea();
    }
    changeDirection() {
        let radian = Math.random() * Math.PI * 2;
        this.dx = Math.sin(radian) * this.speed;
        this.dy = Math.cos(radian) * this.speed;
    }
    calcArea() {
        return this.size * this.size;
    }
}

class Circle extends Figure {
    constructor() {
        super();
        this.x = this.size;
        this.y = this.size;
    }
    calcArea() {
        return super.calcArea() * Math.PI;
    }

    drawCircle() {
        context.beginPath();
        context.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
        context.fillStyle = this.color;
        context.fill();
    }

    updateCircle() {
        if (this.x - this.size === 0 && this.y - this.size === 0) {
            this.changeDirection();
        }
        if (this.x + this.size > canvas.width || this.x - this.size < 0) {
            this.dx = -this.dx;
        }
        if (this.y + this.size > canvas.height || this.y - this.size < 0) {
            this.dy = -this.dy;
        }
        this.x += this.dx;
        this.y += this.dy;
        this.drawCircle();
    }
}

class Square extends Figure {
    drawSquare() {
        context.beginPath();
        context.rect(this.x, this.y, this.size, this.size);
        context.fillStyle = this.color;
        context.fill();
    }

    updateSquare() {
        if (this.x === 0 && this.y === 0) {
            this.changeDirection();
        }
        if (this.x + this.size > canvas.width || this.x < 0) {
            this.dx = - this.dx;
        }
        if (this.y + this.size > canvas.height || this.y < 0) {
            this.dy = - this.dy;
        }
        this.x += this.dx;
        this.y += this.dy;
        this.drawSquare();
    }
}


function animate() {
    requestAnimationFrame(animate);
    context.clearRect(0, 0 , innerWidth, innerHeight);
    for (let i = 0; i < figureArray.length; i++) {
        if (i < 10) {
            figureArray[i].updateCircle();
        }
        else {
            figureArray[i].updateSquare();
        }
    }
}

animate();

setTimeout(function run() {
    if (figureArray.length < 10) {
        let figure = new Circle();
        console.log('Area of this circle is: ' + figure.area);
        figureArray.push(figure);
        setTimeout(run, 5000);
    }
    else if (figureArray.length < 20) {
        let figure = new Square();
        console.log('Area of this square is: ' + figure.area);
        figureArray.push(figure);
        setTimeout(run, 5000);
    }
}, 5000);

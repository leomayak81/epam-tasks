function fillMaze(firstDimension = 6, secondDimension = 7) {
    let numberOfSteps = firstDimension - 1 + secondDimension - 1;
    let number = 1;
    let maze = new Array(firstDimension);
    for (let j = 0; j < firstDimension; j++) {
        maze[j] = new Array(secondDimension);
    }

    function right(count) {
        for (let j = count; j < secondDimension - count; j++) {
            maze[count][j] = number;
            number++;
        }
    }
    function down(count) {
        for (let i = count + 1; i < firstDimension - count; i++) {
            maze[i][secondDimension - count - 1] = number;
            number++;
        }
    }
    function left(count) {
        for (let j = secondDimension - count - 2; j >= count; j--) {
            maze[firstDimension - count - 1][j] = number;
            number++;
        }
    }
    function up(count) {
        for (let i = firstDimension - count - 2; i > count; i--) {
            maze[i][count] = number;
            number++;
        }
    }

    let directions = [right, down, left, up];
    for (let step = 0; step < numberOfSteps; step++) {
        directions[step % 4](parseInt(step / 4));
    }

    return maze;
}

console.log(fillMaze());        //fills 2d array, where firstDimension = 6 and secondDimension = 7.
console.log('\n');
console.log(fillMaze(9, 10));   //fills 2d array, where firstDimension = 9 and secondDimension = 10.
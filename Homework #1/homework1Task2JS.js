function makeGroups(roles, script) {
    let indexes = [];
    let lines = [];

    for (let role of roles) {
        let index;
        let startIndex = 0;
        while (~(index = script.indexOf(role + ':', startIndex))) {
            indexes.push(index);
            startIndex = index + role.length;
        }
    }
    indexes.sort();

    for (let i = 0; i < indexes.length; i++) {
        lines.push(script.slice(indexes[i], indexes[i+1]));
    }
    for (let i = 0; i < lines.length; i++) {
        lines[i] = lines[i].slice(0, lines[i].indexOf(':') + 1) + `${i+1 + ')'}` + lines[i].slice(lines[i].indexOf(':') + 1);
    }
    for (let role of roles) {
        console.log(role + ':');
        for (i = 0; i < lines.length; i++) {
            if (~lines[i].indexOf(role)) {
                console.log(lines[i].slice(role.length + 1));
            }
        }
        console.log('\n');
    }
}

makeGroups(['Городничий','Аммос Федорович','Артемий Филиппович','Лука Лукич'],
    'Городничий: Я, Городничий, пригласил вас, господа, с тем, чтобы сообщить вам пренеприятное известие: к нам едет ревизор.\ ' +
    'Аммос Федорович: Как ревизор? Артемий Филиппович: Как ревизор?\ ' +
    'Городничий: Ревизор из Петербурга, инкогнито. И еще с секретным предписаньем.\ ' +
    'Аммос Федорович: Вот те на! Артемий Филиппович: Вот не было заботы, так подай!\ ' +
    'Лука Лукич: Господи боже! Еще и с секретным предписаньем!');
let btnContainer = document.querySelector(".pizza_menu__btn_container");
let listBtn = btnContainer.querySelector("#list_btn");
let gridBtn = btnContainer.querySelector("#grid_btn");
let priceUpBtn = btnContainer.querySelector("#price_up_btn");
let priceDownBtn = btnContainer.querySelector("#price_down_btn");
let nameUpBtn = btnContainer.querySelector("#name_up_btn");
let nameDownBtn = btnContainer.querySelector("#name_down_btn");
let input = btnContainer.querySelector("#filter_input");
let container = document.querySelector(".pizza_menu__list_container");


let arrayOfNames = ["CHICKEN, BACON & AVOCADO", "CHEESY CHICKEN, BACON & CHORIZO", "LOADED SUPREME",
                    "MEGA MEATLOVERS", "CHICKEN & CAMEMBERT", "PERI PERI CHICKEN", "BBQ CHICKEN & RASHER BACON",
                    "GARLIC PRAWN", "EIGHT MEATS", "FIRE BREATHER", "GODFATHER", "HAWAIIAN","SUPREME", "BBQ MEATLOVERS",
                    "DOUBLE BACON CHEESEBURGER", "PEPPERONI"];

let arrayOfCompositions = ['Succulent seasoned chicken, Avocado, crispy rasher bacon, red onion topped with hollandaise sauce and spring onion.',
                    'Succulent seasoned chicken, Spanish style chorizo, crispy rasher bacon, fire roasted peppers, red onion topped with lots of stretchy mozzarella.',
                    'Ground beef, crispy rasher bacon, mushroom, pepperoni, Italian sausage, fresh baby spinach, smoked leg ham, pineapple, topped with oregano, tomato capsicum sauce & spring onion.',
                    'Succulent chicken, Italian sausage, crispy rasher bacon, ground beef, pepperoni, pork & fennel sausage topped with Hickory BBQ sauce.',
                    'Succulent chicken, camembert, crispy rasher bacon, cherry tomatoes, baby spinach, red onion topped with hollandaise sauce.',
                    'Succulent chicken, cherry tomatoes, baby spinach, red onion topped with peri peri sauce.',
                    'Succulent chicken, rasher bacon, red onion on a BBQ sauce base.',
                    'Prawns, baby spinach, fresh tomato, oregano on a garlic sauce & pizza sauce base.',
                    'Succulent chicken, steak strips, pepperoni, ground beef, smoked leg ham, pork & fennel sausage, Italian sausage, crispy rasher bacon on a BBQ sauce base.',
                    'Pork & fennel sausage, Aussie ground beef, pepperoni, late harvest jalapenos, fresh tomato, red onion topped with chilli flakes.',
                    'Pepperoni, fresh tomato, capsicum, Italian sausage, olives, oregano & garlic sauce.',
                    'Smoked leg ham, pineapple with 50% more mozzarella.',
                    'Pepperoni, rasher bacon, capsicum, ground beef, Italian sausage, mushroom, pineapple, topped with oregano & fresh sliced spring onion.',
                    'Rasher bacon, pepperoni, smoked leg ham, ground beef, Italian sausage on a BBQ sauce base.',
                    'Ground beef, rasher bacon on a BBQ sauce base, topped with mayonnaise.',
                    'Pepperoni & mozzarella.'];

let arrayOfCalories = ['1424.474 kilocalories.', '1330.784 kilocalories.', '1275.335 kilocalories.',
                    '1451.243 kilocalories.', '1420.65 kilocalories.', '1195.029 kilocalories.',
                    '1324.092 kilocalories.', '1051.625 kilocalories.', '1416.826 kilocalories.',
                    '1202.67686 kilocalories.', '1168.26004 kilocalories.', '1173.99618 kilocalories.',
                    '1216.06119 kilocalories.', '1258.1262 kilocalories.', '1340.34417 kilocalories.',
                    '1139.57935 kilocalories.'];

let arrayOfPrices = ['$15.90', '$16.40', '$16.60', '$16.35', '$16.90', '$15.90', '$16.05', '$16.40',
                    '$13.95', '$13.45', '$12.95', '$12.95', '$13.70', '$13.15', '$13.60', '$5.00'];

window.addEventListener("load", function() {
    container.innerHTML = '';
    for (let i = 0; i < 16; i++) {
        let newColumn = document.createElement("div");
        newColumn.setAttribute('class', 'column');

        let newImg = document.createElement("img");
        newImg.setAttribute('src',`img/${i}.png`);
        newColumn.appendChild(newImg);

        let newDescription = document.createElement("div");
        newDescription.setAttribute('class', 'description');
        let newDescriptionName = document.createElement("div");
        newDescriptionName.setAttribute('class', 'description__name');
        newDescriptionName.textContent = arrayOfNames[i];
        newDescription.appendChild(newDescriptionName);
        let newDescriptionComposition = document.createElement("div");
        newDescriptionComposition.setAttribute('class', 'description__composition');
        newDescriptionComposition.textContent = arrayOfCompositions[i];
        newDescription.appendChild(newDescriptionComposition);
        let newDescriptionCalories = document.createElement("div");
        newDescriptionCalories.setAttribute('class', 'description__calories');
        newDescriptionCalories.textContent = arrayOfCalories[i];
        newDescription.appendChild(newDescriptionCalories);
        let newDescriptionPrice = document.createElement("div");
        newDescriptionPrice.setAttribute('class', 'description__price');
        newDescriptionPrice.textContent = arrayOfPrices[i];
        newDescription.appendChild(newDescriptionPrice);

        newColumn.appendChild(newDescription);
        container.appendChild(newColumn);
    }
});

window.addEventListener('load', function() {
    let answer = confirm("Do you want to see a grid?\nPress 'Cancel' to see a list\n");
    if (answer !== true) {
        listView();
    }
});


let elements = document.getElementsByClassName("column");
let composition = document.getElementsByClassName("description__composition");
let calories = document.getElementsByClassName("description__calories");

listBtn.addEventListener("click", listView);
gridBtn.addEventListener("click", gridView);
priceUpBtn.addEventListener("click", sortView);
priceDownBtn.addEventListener("click", sortView);
nameUpBtn.addEventListener("click", sortView);
nameDownBtn.addEventListener("click", sortView);
input.addEventListener("keyup", filterGrid);


function listView() {
    if (listBtn.className.indexOf('active') === -1) {
        listBtn.className += " active";
        gridBtn.className = gridBtn.className.replace(" active", "");
        for (let i = 0; i < elements.length; i++) {
            composition[i].className += " none";
            calories[i].className += " none";
            elements[i].className += " row--list";
        }
    }
}

function gridView() {
    if (gridBtn.className.indexOf('active') === -1) {
        gridBtn.className += " active";
        listBtn.className = listBtn.className.replace(" active", "");
        for (let i = 0; i < elements.length; i++) {
            composition[i].className = composition[i].className.replace(" none", "");
            calories[i].className = calories[i].className.replace(" none", "");
            elements[i].className = elements[i].className.replace("row--list", "");
        }
    }
}

function filterGrid() {
    let filter = input.value.toUpperCase();
    for (let i = 0; i < elements.length; i++) {
        let text = composition[i].textContent;
        if (text.toUpperCase().indexOf(filter) === -1) {
            if ( elements[i].className.indexOf('none') === -1) {
                elements[i].className += " none";
            }
        } else {
            elements[i].className = elements[i].className.replace(" none", "");
        }
    }
}


function sortView(e) {
    if (e.currentTarget.textContent === 'Price ⬆') {
        sortByPrice(true);
    } else if (e.currentTarget.textContent === 'Price ⬇') {
        sortByPrice();
    } else if (e.currentTarget.textContent === 'Name ⬆') {
        sortByName(true);
    } else if (e.currentTarget.textContent === 'Name ⬇') {
        sortByName();
    }

}

function sortByName(reverse) {
    let elementsLength = elements.length;
    let names = document.getElementsByClassName("description__name");
    let namesArray = [].slice.call(names).map(x => x.textContent);
    let sortedNamesArray = [...namesArray].sort();
    if (reverse) {
        sortedNamesArray = sortedNamesArray.reverse();
    }
    for (let i = 0; i < elementsLength; i++) {
        for (let j = 0; j < elementsLength; j++) {
            if (sortedNamesArray[i] === namesArray[j]) {
                container.appendChild(elements[j].cloneNode(true));
                elements[j].className += ' hidden';
            }
        }
    }
    for (let i = 0; i < elementsLength; i++) {
        if (elements[0].className.indexOf('hidden') > -1) {
            container.removeChild(elements[0]);
        }
    }
}

function sortByPrice(reverse) {
    let elementsLength = elements.length;
    let prices = document.getElementsByClassName("description__price");
    let pricesArray = [].slice.call(prices).map(x => x.textContent.trim().slice(1));
    let sortedPricesArray = [...pricesArray].sort((a, b) =>{
        return +a - b;
    });
    if (reverse) {
        sortedPricesArray = sortedPricesArray.reverse();
    }
    for (let i = 0; i < elementsLength; i++) {
        for (let j = 0; j < elementsLength; j++) {
            if (sortedPricesArray[i] === pricesArray[j]) {
                if ( elements[j].className.indexOf('hidden') === -1) {
                    container.appendChild(elements[j].cloneNode(true));
                    elements[j].className += ' hidden';
                }
            }
        }
    }
    for (let i = 0; i < elementsLength; i++) {
        if (elements[0].className.indexOf('hidden') > -1) {
            container.removeChild(elements[0]);
        }
    }
}
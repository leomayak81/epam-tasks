let btnContainer = document.querySelector(".pizza_menu__btn_container");
let listBtn = btnContainer.querySelector("#list_btn");
let gridBtn = btnContainer.querySelector("#grid_btn");
let priceUpBtn = btnContainer.querySelector("#price_up_btn");
let priceDownBtn = btnContainer.querySelector("#price_down_btn");
let nameUpBtn = btnContainer.querySelector("#name_up_btn");
let nameDownBtn = btnContainer.querySelector("#name_down_btn");
let input = btnContainer.querySelector("#filter_input");
let container = document.querySelector(".pizza_menu__list_container");


let arrayOfNames = ["CHICKEN, BACON & AVOCADO", "CHEESY CHICKEN, BACON & CHORIZO", "LOADED SUPREME",
                    "MEGA MEATLOVERS", "CHICKEN & CAMEMBERT", "PERI PERI CHICKEN", "BBQ CHICKEN & RASHER BACON",
                    "GARLIC PRAWN", "EIGHT MEATS", "FIRE BREATHER", "GODFATHER", "HAWAIIAN","SUPREME", "BBQ MEATLOVERS",
                    "DOUBLE BACON CHEESEBURGER", "PEPPERONI"];

let arrayOfCompositions = ['bread, <span>chicken</span>, <span>avocado</span>, <span>rasher bacon</span>, <span>red onion topped with hollandaise sauce</span>.',
                    'bread, <span>chicken</span>, <span>Spanish style chorizo</span>, <span>rasher bacon</span>, <span>fire roasted peppers</span>, <span>red onion topped with lots of stretchy mozzarella</span>.',
                    'bread, <span>ground beef</span>, <span>rasher bacon</span>, <span>mushroom</span>, <span>pepperoni</span>, <span>Italian sausage</span>, <span>baby spinach</span>, <span>smoked leg ham</span>, <span>pineapple, topped with oregano, tomato capsicum sauce & spring onion</span>.',
                    'bread, <span>chicken</span>, <span>Italian sausage</span>, <span>rasher bacon</span>, <span>ground beef</span>, <span>pepperoni</span>, <span>pork & fennel sausage topped with Hickory BBQ sauce</span>.',
                    'bread, <span>chicken</span>, <span>camembert</span>, <span>rasher bacon</span>, <span>cherry tomatoes</span>, <span>baby spinach</span>, <span>red onion topped with hollandaise sauce</span>.',
                    'bread, <span>chicken</span>, <span>cherry tomatoes</span>, <span>baby spinach</span>, <span>red onion topped with peri peri sauce</span>.',
                    'bread, <span>chicken</span>, <span>rasher bacon</span>, <span>red onion on a BBQ sauce base</span>.',
                    'bread, <span>prawns</span>, <span>baby spinach</span>, <span>fresh tomato</span>, <span>oregano on a garlic sauce</span>.',
                    'bread, <span>chicken</span>, <span>steak strips</span>, <span>pepperoni</span>, <span>ground beef</span>, <span>smoked leg ham</span>, <span>pork & fennel sausage</span>, <span>Italian sausage</span>, <span>crispy rasher bacon on a BBQ sauce base</span>.',
                    'bread, <span>pork & fennel sausage</span>, <span>ground beef</span>, <span>pepperoni</span>, <span>late harvest jalapenos</span>, <span>fresh tomato</span>, <span>red onion topped with chilli flakes</span>.',
                    'bread, <span>pepperoni</span>, <span>fresh tomato</span>, <span>capsicum</span>, <span>Italian sausage</span>, <span>olives</span>, <span>oregano on a garlic sauce</span>.',
                    'bread, <span>smoked leg ham</span>, <span>pineapple with 50% more mozzarella</span>.',
                    'bread, <span>pepperoni</span>, <span>rasher bacon</span>, <span>capsicum</span>, <span>ground beef</span>, <span>Italian sausage</span>, <span>mushroom</span>, <span>pineapple, topped with oregano & fresh sliced spring onion</span>.',
                    'bread, <span>rasher bacon</span>, <span>pepperoni</span>, <span>smoked leg ham</span>, <span>ground beef</span>, <span>Italian sausage on a BBQ sauce base</span>.',
                    'bread, <span>ground beef</span>, <span>rasher bacon on a BBQ sauce base, topped with mayonnaise</span>.',
                    'bread, <span>pepperoni</span>, <span>mozzarella</span>.'];

let productEnergyAmount = {
    'bread': [265, 0.25],       // First item of an array is the energy amount in 100g of the corresponding product.
    'olives': [144, 3.75],      // Second item of an array is the price for 100g of the corresponding product in the US dollars.
    'prawns': [120, 21],
    'chicken': [152, 1.58],
    'avocado': [167, 0.81],
    'mushroom': [30, 0.41],
    'capsicum': [40, 0.23],
    'pepperoni': [494, 1.07],
    'camembert': [299, 1.03],
    'mozzarella': [280, 3],
    'ground beef': [332, 12.34],
    'rasher bacon': [193, 0.65],
    'steak strips': [117, 4.95],
    'fresh tomato': [17, 0.2],
    'baby spinach': [23, 0.89],
    'smoked leg ham': [115, 1.71],
    'Italian sausage': [346, 2.82],
    'cherry tomatoes': [25, 0.27],
    'fire roasted peppers': [62, 0.83],
    'Spanish style chorizo': [464, 1.46],
    'pork & fennel sausage': [129, 1.21],
    'late harvest jalapenos': [28, 1.03],
    'oregano on a garlic sauce': [103, 0.87],
    'red onion on a BBQ sauce base': [200, 0.87],
    'pineapple with 50% more mozzarella': [300, 2.06],
    'Italian sausage on a BBQ sauce base': [300, 2.02],
    'red onion topped with chilli flakes': [200, 2.06],
    'red onion topped with peri peri sauce': [400, 2.06],
    'red onion topped with hollandaise sauce': [570, 2.06],
    'crispy rasher bacon on a BBQ sauce base': [370, 0,70],
    'red onion topped with lots of stretchy mozzarella': [570, 2.06],
    'pork & fennel sausage topped with Hickory BBQ sauce': [300, 0.9],
    'rasher bacon on a BBQ sauce base, topped with mayonnaise': [600, 1.2],
    'pineapple, topped with oregano, tomato capsicum sauce & spring onion': [300, 2.06],
};

let arrayOfCalories = ['1877 kilocalories.', '2236 kilocalories.', '2628 kilocalories.',
                    '2612 kilocalories.', '2057 kilocalories.', '1395 kilocalories.',
                    '1340 kilocalories.', '1058 kilocalories.', '2850 kilocalories.',
                    '1995 kilocalories.', '1939 kilocalories.', '1210 kilocalories.',
                    '2230 kilocalories.', '2229 kilocalories.', '1727 kilocalories.',
                    '1569 kilocalories.'];


let arrayOfPrices = ['$25.90', '$26.40', '$42.60', '$40.35', '$26.90', '$25.90', '$26.05', '$43.40',
                    '$45.95', '$38.45', '$28.95', '$22.95', '$37.70', '$38.15', '$33.60', '$25.00'];

window.addEventListener("load", function() {
    for (let i = 0; i < 16; i++) {
        let newColumnContainer = document.createElement("div");
        newColumnContainer.setAttribute('class', 'column_container');
        let newColumn = document.createElement("div");
        newColumn.setAttribute('class', 'column');

        let newImg = document.createElement("img");
        newImg.setAttribute('src',`img/${i}.png`);
        newImg.setAttribute('class', 'column__img');

        let newDescription = document.createElement("div");
        newDescription.setAttribute('class', 'description');
        let newDescriptionName = document.createElement("div");
        newDescriptionName.setAttribute('class', 'description__name');
        newDescriptionName.textContent = arrayOfNames[i];
        newDescription.appendChild(newDescriptionName);
        let newDescriptionComposition = document.createElement("div");
        newDescriptionComposition.setAttribute('class', 'description__composition');
        newDescriptionComposition.innerHTML = arrayOfCompositions[i];
        newDescription.appendChild(newDescriptionComposition);
        let newDescriptionCalories = document.createElement("div");
        newDescriptionCalories.setAttribute('class', 'description__calories');
        newDescriptionCalories.textContent = arrayOfCalories[i];
        newDescription.appendChild(newDescriptionCalories);
        let newDescriptionPrice = document.createElement("div");
        newDescriptionPrice.setAttribute('class', 'description__price');
        newDescriptionPrice.textContent = arrayOfPrices[i];
        newDescription.appendChild(newDescriptionPrice);

        newColumn.appendChild(newDescription);
        newColumnContainer.appendChild(newImg);
        newColumnContainer.appendChild(newColumn);
        container.appendChild(newColumnContainer);

    }
});

window.addEventListener('load', function() {
    let answer = confirm("Do you want to see a grid?\nPress 'Cancel' to see a list\n");
    if (answer !== true) {
        listView();
    }
});



let elements = document.getElementsByClassName("column_container");
let composition = document.getElementsByClassName("description__composition");
let calories = document.getElementsByClassName("description__calories");



listBtn.addEventListener("click", listView);
gridBtn.addEventListener("click", gridView);
priceUpBtn.addEventListener("click", sortView);
priceDownBtn.addEventListener("click", sortView);
nameUpBtn.addEventListener("click", sortView);
nameDownBtn.addEventListener("click", sortView);
input.addEventListener("keyup", filterGrid);
container.addEventListener('click', animate);
container.addEventListener('mouseover', removeComponent);


function removeComponent(event) {
    if (event.target.nodeName === 'SPAN') {
        if (event.target.className.indexOf(' line_through') === -1) {
            event.target.className += ' line_through';
            for (let product in productEnergyAmount) {
                if (event.target.textContent === product) {
                    event.target.parentElement.parentElement.childNodes[2].textContent =
                        `${+event.target.parentElement.parentElement.childNodes[2].textContent.slice(0, 4) -
                        productEnergyAmount[product][0]}` + ' kilocalories';
                    event.target.parentElement.parentElement.childNodes[3].textContent = '$' +
                        `${+event.target.parentElement.parentElement.childNodes[3].textContent.slice(1, 6) -
                        productEnergyAmount[product][1]}`;
                }
            }
        } else {
            event.target.className = event.target.className.replace(" line_through", "");
            for (let product in productEnergyAmount) {
                if (event.target.textContent === product) {
                    event.target.parentElement.parentElement.childNodes[2].textContent =
                        `${+event.target.parentElement.parentElement.childNodes[2].textContent.slice(0, 4) +
                        productEnergyAmount[product][0]}` + ' kilocalories';
                    event.target.parentElement.parentElement.childNodes[3].textContent = '$' +
                        `${+event.target.parentElement.parentElement.childNodes[3].textContent.slice(1, 6) +
                        productEnergyAmount[product][1]}`;
                }
            }
        }
    }
}

function animate(event) {
    if (listBtn.className.indexOf('active') === -1) {
        if (event.target.closest('.column').className.indexOf('animate') === -1) {
            event.target.closest('.column').className += ' animate';
            event.target.closest('.column').parentElement.firstElementChild.className += ' animate';
        }

        let picture = event.target.closest('.column').parentElement.firstElementChild;
        let newPicture = picture.cloneNode(true);
        picture.parentNode.replaceChild(newPicture, picture);

        let text = event.target.closest('.column');
        let newText = text.cloneNode(true);
        text.parentNode.replaceChild(newText, text);
    }
}

function listView() {
    if (listBtn.className.indexOf('active') === -1) {
        listBtn.className += " active";
        gridBtn.className = gridBtn.className.replace(" active", "");
        for (let i = 0; i < elements.length; i++) {
            composition[i].className += " none";
            calories[i].className += " none";
            elements[i].className += " row--list";
        }
    }
}

function gridView() {
    if (gridBtn.className.indexOf('active') === -1) {
        gridBtn.className += " active";
        listBtn.className = listBtn.className.replace(" active", "");
        for (let i = 0; i < elements.length; i++) {
            composition[i].className = composition[i].className.replace(" none", "");
            calories[i].className = calories[i].className.replace(" none", "");
            elements[i].className = elements[i].className.replace("row--list", "");
        }
    }
}

function filterGrid() {
    let filter = input.value.toUpperCase();
    for (let i = 0; i < elements.length; i++) {
        let text = composition[i].textContent;
        if (text.toUpperCase().indexOf(filter) === -1) {
            if ( elements[i].className.indexOf('none') === -1) {
                elements[i].className += " none";
            }
        } else {
            elements[i].className = elements[i].className.replace(" none", "");
        }
    }
}


function sortView(e) {
    if (e.currentTarget.textContent === 'Price ⬆') {
        sortByPrice(true);
    } else if (e.currentTarget.textContent === 'Price ⬇') {
        sortByPrice();
    } else if (e.currentTarget.textContent === 'Name ⬆') {
        sortByName(true);
    } else if (e.currentTarget.textContent === 'Name ⬇') {
        sortByName();
    }

}

function sortByName(reverse) {
    let elementsLength = elements.length;
    let names = document.getElementsByClassName("description__name");
    let namesArray = [].slice.call(names).map(x => x.textContent);
    let sortedNamesArray = [...namesArray].sort();
    if (reverse) {
        sortedNamesArray = sortedNamesArray.reverse();
    }
    for (let i = 0; i < elementsLength; i++) {
        for (let j = 0; j < elementsLength; j++) {
            if (sortedNamesArray[i] === namesArray[j]) {
                let newNode = elements[j].cloneNode(true);
                newNode.childNodes[0].className = newNode.childNodes[0].className.replace(" animate", "");
                newNode.childNodes[1].className = newNode.childNodes[1].className.replace(" animate", "");
                container.appendChild(newNode);
                elements[j].className += ' hidden';
            }
        }
    }
    for (let i = 0; i < elementsLength; i++) {
        if (elements[0].className.indexOf('hidden') > -1) {
            container.removeChild(elements[0]);
        }
    }
}

function sortByPrice(reverse) {
    let elementsLength = elements.length;
    let prices = document.getElementsByClassName("description__price");
    let pricesArray = [].slice.call(prices).map(x => x.textContent.trim().slice(1));
    let sortedPricesArray = [...pricesArray].sort((a, b) => +a - b);
    if (reverse) {
        sortedPricesArray = sortedPricesArray.reverse();
    }
    for (let i = 0; i < elementsLength; i++) {
        for (let j = 0; j < elementsLength; j++) {
            if (sortedPricesArray[i] === pricesArray[j]) {
                if ( elements[j].className.indexOf('hidden') === -1) {
                    let newNode = elements[j].cloneNode(true);
                    newNode.childNodes[0].className = newNode.childNodes[0].className.replace(" animate", "");
                    newNode.childNodes[1].className = newNode.childNodes[1].className.replace(" animate", "");
                    container.appendChild(newNode);
                    elements[j].className += ' hidden';
                }
            }
        }
    }
    for (let i = 0; i < elementsLength; i++) {
        if (elements[0].className.indexOf('hidden') > -1) {
            container.removeChild(elements[0]);
        }
    }
}